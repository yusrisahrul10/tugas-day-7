﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using tugas_day_7.Logics;
using tugas_day_7.Models;
using tugas_day_7.Models.Get;

namespace tugas_day_7.Controllers
{
    [Route("api/tasks")]
    [ApiController]
    public class UserWithTaskController : ControllerBase
    {
        [HttpPost]
        [Route("AddUserWithTask")]
        public ActionResult InsertUserAndTask([FromBody] UserWithTask body)
        {
            try
            {
                DBLogic.InsertUserAndTasks(body);
                return StatusCode(201, "created");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetUserWithTask")]
        public ActionResult GetUserWithTask([FromQuery] string? name)
        {
            try
            {
                List<DetailTask> result = new List<DetailTask>();
                result = DBLogic.GetUserWithTaskByName(name);

                return Ok(result);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}
