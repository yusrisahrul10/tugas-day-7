using tugas_day_7.Logics;
using tugas_day_7.Models;
using static tugas_day_7.Logics.CustomModelBinder;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//builder.Services.AddControllers(opt => {
//  opt.ModelBinderProviders.Insert(0, new MyCustomBinderProvider());
//});

IConfiguration configuration = builder.Configuration;
CRUD.GetConfiguration(configuration);
DBLogic.GetConfiguration(configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
