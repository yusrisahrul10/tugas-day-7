﻿namespace tugas_day_7.Models
{
    public class UserWithTask
    {
        public string name { get; set; }
        public List<TaskDetail> tasks { get; set; }
    }

    public class TaskDetail
    {
        public string task_detail { get; set; }
    }
}
