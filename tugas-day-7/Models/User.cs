﻿namespace tugas_day_7.Models
{
    public class User
    {
        public int pk_users_id { get; set; }

        public string name { get; set; }

        public List<TaskUser> tasks { get; set; }
    }
}
