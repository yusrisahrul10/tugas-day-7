﻿namespace tugas_day_7.Models.Get
{
    public class DetailTask
    {
        public List<DetailTaskUser> tasks { get; set; }

        public int pk_users_id { get; set; }

        public string name { get; set; }

    }

    public class DetailTaskUser
    {
        public int pk_tasks_id { get; set; }

        public string task_detail { get; set; }
    }
}
