﻿namespace tugas_day_7.Models
{
    public class TaskUser
    {
        public int pk_tasks_id { get; set; }
           
        public string task_detail { get; set; }

        public string fk_users_id { get; set; }

        public User user { get; set; }

    }
}
