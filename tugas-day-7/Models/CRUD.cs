﻿using System.Data;
using System.Data.SqlClient;

namespace tugas_day_7.Models
{
    public class CRUD
    {

        private static string connectionString = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
        }

        #region ExecuteNonQuery
        public static int ExecuteNonQuery(string query)
        {
            int result = 0;

            // begin connection
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                #region query process to database
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    result = cmd.ExecuteNonQuery(); // ExecuteNonQuery untuk query yang tidak return apa-apa
                }
                #endregion

                // close connection
                con.Close();
            }

            return result;
        }

        #endregion

        public static DataTable ExecuteQuery(string sql, SqlParameter[] sqlParameters = null)
        {
            DataTable result = new DataTable();

            #region query process process database
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                #region query 
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    if (sqlParameters != null) { cmd.Parameters.AddRange(sqlParameters); }

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(result);
                }
                conn.Close();
                #endregion
            }
            #endregion

            return result;
        }
    }
}
