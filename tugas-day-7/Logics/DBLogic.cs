﻿using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using tugas_day_7.Models;
using tugas_day_7.Models.Get;

namespace tugas_day_7.Logics
{
    public class DBLogic
    {
        private static string connectionString = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
        }

        public static string InsertUserAndTasks(UserWithTask userWithTasks)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        cmd.CommandText = "INSERT INTO Users ([name]) values (@name); select SCOPE_IDENTITY();";
                        cmd.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar) { Value = userWithTasks.name });

                        decimal pk_user_id = (decimal)cmd.ExecuteScalar();

                        cmd.Parameters.Clear();

                        foreach (var taskdetail in userWithTasks.tasks)
                        {
                            cmd.CommandText = "INSERT INTO Tasks (task_detail, fk_users_id) VALUES (@taskdetail, @fkuser);";
                            cmd.Parameters.Add(new SqlParameter("@taskdetail", SqlDbType.VarChar) { Value = taskdetail.task_detail ?? "" });
                            cmd.Parameters.Add(new SqlParameter("@fkuser", SqlDbType.Int) { Value = pk_user_id });

                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                        }
                        cmd.Transaction.Commit();
                        conn.Close();
                        return "Success";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        conn.Close();
                        throw ex;
                    }
                }
            }
        }

        public static List<DetailTask> GetUserWithTaskByName(string name = "")
        {
            Dictionary<int, DetailTask> detailTaskMap = new Dictionary<int, DetailTask>();

            string query = "select * from users join tasks on users.pk_users_id = tasks.fk_users_id";

            if (!String.IsNullOrEmpty(name))
            {
                query += " where name like @name";
            }

            SqlParameter[] sqlparams = new SqlParameter[]
            {
                new SqlParameter("@name",SqlDbType.VarChar){Value = "%" + (name ?? "") + "%" }
            };

            DataTable dataTable = CRUD.ExecuteQuery(query, sqlparams);
            foreach (DataRow row in dataTable.Rows)
            {
                DetailTask existingDetailTask;
                if (!detailTaskMap.TryGetValue((int)row["pk_users_id"], out existingDetailTask))
                {
                    existingDetailTask = new DetailTask
                    {
                        pk_users_id = (int)row["pk_users_id"],
                        name = (string)row["name"],
                        tasks = new List<DetailTaskUser>()
                    };
                    detailTaskMap.Add((int)row["pk_users_id"], existingDetailTask);
                }

                existingDetailTask.tasks.Add(new DetailTaskUser
                {
                    pk_tasks_id = (int)row["pk_tasks_id"],
                    task_detail = (string)row["task_detail"]
                });

            }

            return detailTaskMap.Values.ToList();
        }
    }
}
