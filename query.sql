CREATE DATABASE tugasday7;

USE tugasday7;

CREATE TABLE Users(
	pk_users_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	name VARCHAR(255) NOT NULL,
);

CREATE TABLE Tasks(
	pk_tasks_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	task_detail VARCHAR(MAX) NOT NULL,
	fk_users_id INT NOT NULL
);

DECLARE @UserId INT INSERT INTO Users (name) VALUES ('trainer'); SET @UserId = SCOPE_IDENTITY();

INSERT INTO Tasks (task_detail, fk_users_id) VALUES ('teach passion with love', @UserId);

select * from users join tasks on users.pk_users_id = tasks.fk_users_id;

select * from users join tasks on users.pk_users_id = tasks.fk_users_id where name like '%trainee%';